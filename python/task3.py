from random import randint

simple_map = {}

five_hundred_nums = []
hundred_nums = []


class Var:
    def __init__(self, index):
        self.index = index
        self.element = -1
        self.value = 0


def create_vars():
    for i in range(100):
        hundred_nums.append(Var(randint(1, 1000000)))

    for i in range(500):
        five_hundred_nums.append(Var(randint(1, 1000000)))


def start_stream(count: int):
    for i in range(count):
        random_int = randint(1, 1000)

        if simple_map.get(random_int) is None:
            simple_map[random_int] = 1
        else:
            simple_map[random_int] += 1

        for var in five_hundred_nums:
            if var.index == i:
                var.element = random_int
                var.value = 1
            else:
                if var.index < i:
                    if var.element == random_int:
                        var.value += 1

        for var in hundred_nums:
            if var.index == i:
                var.element = random_int
                var.value = 1
            else:
                if var.index < i:
                    if var.element == random_int:
                        var.value += 1


def count_simple_moments():
    f_zero = len(simple_map)

    f_one = 0
    f_two = 0
    for var in simple_map:
        f_one += simple_map[var]
        f_two += simple_map[var] * simple_map[var]

    print("Zero moment:\t" + str(f_zero))
    print("First moment:\t" + str(f_one))
    print("Second moment:\t" + str(f_two))


def count_moments_f_2_by_algorithm():
    print("Second moment by algorithm")

    answer = 0
    for var in hundred_nums:
        answer += var.value * 2 - 1

    print("100 variables:\t" + str(answer * 1000000 / 100))

    answer = 0
    for var in five_hundred_nums:
        answer += var.value * 2 - 1

    print("500 variables:\t" + str(answer * 1000000 / 500))



if __name__ == '__main__':
    create_vars()
    create_vars()
    start_stream(1000000)
    count_simple_moments()
    count_moments_f_2_by_algorithm()