import re

import psycopg2
import vk_api

token = "9f7d09b99f7d09b99f7d09b9ce9f0b923999f7d9f7d09b9ff4ad1053c4a0cd24b6e9431"

connection = psycopg2.connect(dbname='postgres', user='postgres', password='QAZwsx123456',
                              host='db-datamining.c0tho0a1qim6.us-east-1.rds.amazonaws.com')
connection.autocommit = True


def start(group_id, count) -> list:
    session = vk_api.VkApi(token=token)
    api = session.get_api()
    return get_post('-' + group_id, api=api, count=count)


def get_post(group_id, api, count: int) -> list:
    i = 0
    num_of_post = 0
    posts_list = []
    while i < (count / 100):
        posts = api.wall.get(owner_id=group_id, offset=num_of_post, count=1)
        for items in posts['items']:
            posts_list.append(items['text'])
            num_of_post += 1
        i += 1
    return posts_list


def letters_count(posts: list) -> dict:
    word_dic = {}
    reg = re.compile('[^а-яА-Я ]')
    for post in posts:
        for word in post.strip().split(' '):
            word = reg.sub('', word).lower()
            if word in word_dic.keys():
                word_dic[word] += 1
            else:
                word_dic[word] = 1
    return word_dic


def out_txt_words_count(word_dic: dict):
    word_dic = {k: v for k, v in sorted(word_dic.items(), key=lambda item: item[1])}
    with open('answer.txt', 'w') as file:
        for key in word_dic.keys():
            file.write(key + ': ' + str(word_dic[key]) + '\n')


def truncate_db(cursor):
    #language=SQL
    truncate = 'truncate table words'
    cursor.execute(truncate)


def insert_into_database(word_dic: dict, cursor):
    #language=SQL
    insert_row = 'insert into words(word, count) values({}, {});'
    for key in word_dic.keys():
        if key not in ['', ' ', '\n', '\s']:
            cursor.execute(insert_row.format('\'' + key + '\'', word_dic[key]))


def run():
    cursor = connection.cursor()
    truncate_db(cursor)
    insert_into_database(letters_count(start('35488145', 1)), cursor)

