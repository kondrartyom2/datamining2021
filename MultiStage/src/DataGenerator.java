import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataGenerator {

    public Integer random(Integer max, Integer min) {
        return (int) (Math.random( ) * (max - min)) + min;
    }

    public void generate() throws IOException {

        BufferedWriter bw = new BufferedWriter(
                new FileWriter("src/answer.txt")
        );

        BufferedReader br = new BufferedReader(
                new FileReader("src/products")
        );

        List<String> list = new ArrayList<>();

        while (br.ready()) {
            String str = br.readLine();
            list.add(str);
        }

        for (int i = 0; i < 1000; i++) {
            for (int j = 0; j < random(7, 1); j++) {
                bw.write(list.get(random(list.size() - 1, 0)) + " ");
            }
            bw.write("\n");
        }
        bw.flush();
    }
}
