import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class MultiStage {

    List<List<Integer>> list;
    Map<Integer, Integer> hashMap = new HashMap<>();
    Map<String, Integer> intMap = new HashMap<>();
    Map<Integer, String> stringMap = new HashMap<>();
    Integer support;

    public MultiStage(List<List<String>> list, Integer support) {
        this.list = postConstruct(list);
        this.support = support;
    }

    public List<List<Integer>> postConstruct(List<List<String>> list) {

        Integer i = 1;
        List<List<Integer>> answerList = new ArrayList<>();

        for (List<String> lst : list) {
            for (String s : lst) {
                if (!intMap.containsKey(s)) {
                    intMap.put(s, i);
                    stringMap.put(i, s);
                    i++;
                }
                if (!hashMap.containsKey(intMap.get(s))) {
                    hashMap.put(intMap.get(s), 0);
                }
                hashMap.remove(intMap.get(s));
                hashMap.put(intMap.get(s), hashMap.get(intMap.get(s)) + 1);
            }
        }

        for (List<String> lst : list) {
            answerList.add(lst.stream().map(s -> intMap.get(s)).collect(Collectors.toList()));
        }

        return answerList;
    }

    public List<String> analyze() {

        List<String> answerString = new ArrayList<>();

        for (int i = 1; i <= hashMap.size(); i++) {
            if (hashMap.get(i) >= support) {
                answerString.add(stringMap.get(i));
            }
        }

        multiStage().forEach(t -> answerString.add(
                stringMap.get(t.x) + "_" + stringMap.get(t.y)
        ));

        return answerString;
    }

    public List<Twin> multiStage() {

        List<Twin> twinList = new ArrayList<>();

        for (List<Integer> lst : list) {
            for (int i = 0; i < lst.size(); i++) {
                for (int j = i + 1; j < lst.size(); j++) {
                    twinList.add(new Twin(lst.get(i), lst.get(j)));
                }
            }
        }

        List<Set<Twin>> list = integerListMap(twinList, hashMap.size(), 1, 1);

        List<Twin> currentParList = getGoodPair(list, twinList);

        list = integerListMap(currentParList, hashMap.size(), 1, 2);

        currentParList = getGoodPair(list, twinList);

        List<Twin> answerList = new ArrayList<>();
        Set<Twin> answerSet = new HashSet<>();

        for (Twin twin : currentParList) {
            int x = hashMap.get(twin.x);
            int y = hashMap.get(twin.y);
            if (x < support || y < support) {

            } else {
                if (!answerSet.stream().anyMatch(k -> k.x == twin.x && k.y == twin.y)) {
                    answerSet.add(twin);
                    answerList.add(twin);
                }
            }
        }

        return answerList;
    }

    private List<Twin> getGoodPair(List<Set<Twin>> list, List<Twin> twinList) {

        Set<Twin> twinsSet = new HashSet<>();
        List<Twin> currentParList = new ArrayList<>();

        for (Set<Twin> twins : list) {
            Iterator iterator = twins.iterator();

            int currentSum = 0;

            while (iterator.hasNext()) {
                Twin twin = (Twin) iterator.next();
                currentSum += twin.count;
            }

            if (currentSum >= support) {
                twinsSet.addAll(twins);
            }
        }

        for (Twin twin : twinList) {
            if (twinsSet.stream().anyMatch(k -> k.x == twin.x && k.y == twin.y)) {
                currentParList.add(twin);
            }
        }

        return currentParList;
    }

    public List<Set<Twin>> integerListMap(List<Twin> list, int hash, int x, int y) {

        List<Set<Twin>> answerList = new ArrayList<>();

        for (int i = 0; i < hash; i++) {
            answerList.add(new HashSet<>());
        }

        for (Twin twin : list) {

            int hashPair = (x * twin.x + y * twin.y) % hash;

            Iterator iterator = answerList.get(hashPair).iterator();
            int k = 0;
            while (iterator.hasNext()) {
                Twin twinNext = (Twin) iterator.next();
                if (twinNext.x == twin.x && twinNext.y == twin.y) {
                    k = twinNext.count;
                }
            }
            answerList.get(hashPair).removeIf(key -> key.y == twin.y && key.x == twin.x);
            twin.count = k + 1;
            answerList.get(hashPair).add(twin);

        }

        return answerList;
    }
}