import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException {

        DataGenerator dataGenerator = new DataGenerator();

        dataGenerator.generate();

        BufferedReader br = new BufferedReader(
                new FileReader("src/answer.txt"));

        List<List<String>> count = new ArrayList<>();

        while (br.ready()) {

            String[] str = br.readLine().split(" ");

            count.add(Arrays.stream(str).collect(Collectors.toList()));
        }

        MultiStage multiStage = new MultiStage(count, 70);

        multiStage.analyze().forEach(System.out::println);
    }
}